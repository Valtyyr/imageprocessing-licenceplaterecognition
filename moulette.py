# -*- coding: utf8 -*-

import os, subprocess
from sys import argv

script, ocr, debug, dirname = argv

print script
print dirname

good = 0
bad = 0
exe = './ImageProcessing/Debug/ImageProcessing'

for file in os.listdir(dirname):
    print 'fichier :' + file
    if debug == "debug" and ocr == "ocr":
        subprocess.call([exe, '--ocr', '--debug', dirname + '/' + file])
    elif ocr == "ocr":
        subprocess.call([exe, '--ocr', dirname + '/' + file])
    elif debug == "debug":
        subprocess.call([exe, '--ocr', '--debug', dirname + '/' + file])
    else:
        subprocess.call([exe, dirname + '/' + file])
