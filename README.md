# README #

### Authors ###

* Valentin 'Valtyr' Macheret
* Mickael Tavares

### Overview ###

This C++/OpenCV project allow to localize licence plate in classic image (.png, .jpg, .jpeg...). All methods are handwritten. OpenCV is use only for input/output handling.

### LPR run steps ###

* Application of a grayscale filter on the original image.
* Projection based on Y axes.
* Projection based on X axes.
* Selection of ROI based on which can content enough symbol to be a licence plate.
* Selection of the final ROI which the ratio (width over high) is the closest to 4.72.