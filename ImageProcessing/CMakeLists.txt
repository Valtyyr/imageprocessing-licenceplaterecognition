cmake_minimum_required (VERSION 2.8)

file (GLOB MAIN_APP src/tools/tools.cpp src/filters/filter.cpp src/detection/detection.cpp src/detection/text_detection.cpp src/detection/lp_detection.cpp src/main/main.cpp)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_executable (ImageProcessing ${MAIN_APP})

target_link_libraries(ImageProcessing ${OpenCV_LIBS})
