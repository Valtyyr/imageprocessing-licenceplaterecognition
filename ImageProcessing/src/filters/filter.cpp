/*
 * Filters.cpp
 *
 *  Created on: 19 juin 2014
 *      Author: valentin
 */

# include "filter.h"

namespace flt {
	void
	sobelFilter(const cv::Mat& iSrc, cv::Mat& iDst) {
		for (int y = 0; y < iSrc.rows; y++) {
			for (int x = 0; x < iSrc.cols; x++) {
				iDst.at<uchar>(y, x) = 0.0;
			}
		}

		for (int y = 1; y < iSrc.rows - 1; y++) {
			for (int x = 1; x < iSrc.cols - 1; x++) {
				int lGx = tool::convolutionXSobel(iSrc, x, y);
				int lGy = tool::convolutionYSobel(iSrc, x, y);
				int lSum = abs(lGx) + abs(lGy);
				lSum = lSum > 255 ? 255 : lSum;
				lSum = lSum < 0 ? 0 : lSum;
				iDst.at<uchar>(y, x) = lSum;
			}
		}
	}

	void
	horizontalEdgeDetection(const cv::Mat& iSrc, cv::Mat& iDst) {
		for (int y = 0; y < iSrc.rows; y++) {
			for (int x = 0; x < iSrc.cols; x++)
				iDst.at<uchar>(y, x) = 0.0;
		}

		for (int y = 1; y < iSrc.rows - 1; y++) {
			for (int x = 1; x < iSrc.cols - 1; x++) {
				int lConv = tool::convolutionXDetection(iSrc, x, y);
				int lSum = abs(lConv);
				lSum = lSum > 255 ? 255 : lSum;
				lSum = lSum < 0 ? 0 : lSum;
				iDst.at<uchar>(y, x) = lSum;
			}
		}
	}

	void
	verticalEdgeDetection(const cv::Mat& iSrc, cv::Mat& iDst) {
		for (int y = 0; y < iSrc.rows; y++) {
			for (int x = 0; x < iSrc.cols; x++)
				iDst.at<uchar>(y, x) = 0.0;
		}

		for (int y = 1; y < iSrc.rows - 1; y++) {
			for (int x = 1; x < iSrc.cols - 1; x++) {
				int lConv = tool::convolutionYDetection(iSrc, x, y);
				int lSum = abs(lConv);
				lSum = lSum > 255 ? 255 : lSum;
				lSum = lSum < 0 ? 0 : lSum;
				iDst.at<uchar>(y, x) = lSum;
			}
		}
	}

	void
	medianFilter(const cv::Mat& iSrc, cv::Mat& iDst) {
		int lWindow[9];

		iDst = iSrc.clone();
		for (int y = 0; y < iSrc.rows; y++) {
			for (int x = 0; x < iSrc.cols; x++) {
				iDst.at<uchar>(y, x) = 0.0;
			}
		}

		for (int y = 1; y < iSrc.rows - 1; y++) {
			for (int x = 1; x < iSrc.cols - 1; x++) {
				lWindow[0] = iSrc.at<uchar>(y - 1, x - 1);
				lWindow[1] = iSrc.at<uchar>(y, x - 1);
				lWindow[2] = iSrc.at<uchar>(y + 1, x - 1);
				lWindow[3] = iSrc.at<uchar>(y - 1, x);
				lWindow[4] = iSrc.at<uchar>(y, x);
				lWindow[5] = iSrc.at<uchar>(y + 1, x);
				lWindow[6] = iSrc.at<uchar>(y - 1, x + 1);
				lWindow[7] = iSrc.at<uchar>(y, x + 1);
				lWindow[8] = iSrc.at<uchar>(y + 1, x + 1);

				tool::insertionSort(lWindow);
				iDst.at<uchar>(y, x) = lWindow[4];
			}
		}
	}

	void
	otsuFilter(const cv::Mat& iSrc, cv::Mat& iDst, int iThreshold) {
		for (int y = 0; y < iSrc.rows; ++y) {
			for (int x = 0; x < iSrc.cols; ++x) {
				iDst.at<uchar>(y, x) = iSrc.at<uchar>(y, x) < iThreshold ? 0 : 255;
			}
		}
	}

	void
	grayScaleFilter(const cv::Mat& iSrc, cv::Mat& iDst) {
		for (int y = 0; y < iSrc.rows; ++y) {
			for (int x = 0; x < iSrc.cols; ++x) {
				int lBlue = iSrc.at<cv::Vec3b>(y,x)[0];
				int lGreen = iSrc.at<cv::Vec3b>(y,x)[1];
				int lRed = iSrc.at<cv::Vec3b>(y,x)[2];
				int lGray = (lRed * 299 + lGreen * 587 + lBlue * 114) / 1000;
				iDst.at<uchar>(y, x) = lGray;
			}
		}
	}
}
