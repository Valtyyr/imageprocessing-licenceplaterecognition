/*
 * Filters.h
 *
 *  Created on: 19 juin 2014
 *      Author: valentin
 */

#ifndef FILTERS_H_
# define FILTERS_H_

# include "../main/all.h"

namespace flt {

	/**
	 * \brief Process a Sobel filter over the source image
	 * \param iSrc the original image
	 * \param iDst the image where will be apply the filter
	 */
	void sobelFilter(const cv::Mat& iSrc, cv::Mat& iDst);

	/**
	 * \brief Process an horizontal edge filter over the source image
	 * \param iSrc the original image
	 * \param iDst the image where will be apply the filter
	 */
	void horizontalEdgeDetection(const cv::Mat& iSrc, cv::Mat& iDst);

	/**
	 * \brief Process a vertical edge filter over the source image
	 * \param iSrc the original image
	 * \param iDst the image where will be apply the filter
	 */
	void verticalEdgeDetection(const cv::Mat& iSrc, cv::Mat& iDst);

	/**
	 * \brief Process a median filter over the source image
	 * \param iSrc the original image
	 * \param iDst the image where will be apply the filter
	 */
	void medianFilter(const cv::Mat& iSrc, cv::Mat& iDst);

	/**
	 * \brief Process an Otsu filter over the source image
	 * \param iSrc the original image
	 * \param iDst the image where will be apply the filter
	 */
	void otsuFilter(const cv::Mat& iSrc, cv::Mat& iDst, int iThreshold);

	/**
	 * \brief Process a grayscale filter over the source image
	 * \param iSrc the original image
	 * \param iDst the image where will be apply the filter
	 */
	void grayScaleFilter(const cv::Mat& iSrc, cv::Mat& iDst);
};

#endif /* FILTERS_H_ */
