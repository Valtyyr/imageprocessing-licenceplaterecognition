/*
 * SquareDetection.h
 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */

#ifndef LPDETECTION_H_
# define LPDETECTION_H_

# include "detection.h"
# include "text_detection.h"
# include "../filters/filter.h"

class LPDetection: public Detection {
public:
	/**
	 * Constructor
	 */
	LPDetection();

	/**
	 * Destructor
	 */
	virtual ~LPDetection();

	/**
	 * \brief Process the detection
	 * \param iImage the original image
	 * \param iDebugMode specifies if the debug mode is active
	 * \param iDebugMode specifies if the orc mode is active
	 */
	virtual void processDetection(cv::Mat iImage, bool iDebugMode, bool iOcrMode);
};

#endif /* LPEDETECTION_H_ */
