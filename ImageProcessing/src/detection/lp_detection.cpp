/*
 * LPDetection.cpp
 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */

# include "lp_detection.h"

LPDetection::LPDetection() {}

LPDetection::~LPDetection() {}

void
selectVerticalPeakProjection(std::vector<int>& iYProjectionConvolution, std::vector<std::pair<int,int>>& iBands, float iCoefficient) {
	std::vector<int> lYProjectionCopy(iYProjectionConvolution);
	int lNbBandsToBeDetected = 3;
	while (lNbBandsToBeDetected > 0) {
		std::vector<int>::iterator lIterator = std::max_element(lYProjectionCopy.begin(), lYProjectionCopy.end());
		int lYBm = std::find(lYProjectionCopy.begin(), lYProjectionCopy.end(), *lIterator) - lYProjectionCopy.begin();
		int lYB0 = lYBm;
		for (; lYProjectionCopy[lYB0] >= iCoefficient * lYProjectionCopy[lYBm]; lYB0--);

		int lYB1 = lYBm;
		for (; lYProjectionCopy[lYB1] >= iCoefficient * lYProjectionCopy[lYBm]; lYB1++);
		lYB1--;

		if (lYProjectionCopy[lYB0] == 0) {
			for (unsigned i = 0; i < iBands.size(); ++i) {
				if (std::abs(lYB0 - iBands[i].second) <= 1) {
					iBands[i] = std::pair<int, int>(iBands[i].first, lYB1);
				}
			}
		} else if (lYProjectionCopy[lYB1] == 0) {
			for (unsigned i = 0; i < iBands.size(); ++i) {
				if (std::abs(lYB1 - iBands[i].first) <= 1) {
					iBands[i] = std::pair<int, int>(lYB0, iBands[i].second);
				}
			}
		} else
			iBands.push_back(std::make_pair(lYB0,lYB1));

		for (int i = lYB0; i <= lYB1; ++i)
			lYProjectionCopy[i] = 0;

		lNbBandsToBeDetected--;
	}
}

void
selectHorizontalPeakProjection(std::vector<std::vector<int>> iXProjectionConvolution,
				 std::vector<std::pair<int, int>>& iPlates, float iCoefficient) {
	for (std::vector<int> lXProjection : iXProjectionConvolution) {
		std::vector<int>::iterator lIterator = std::max_element(lXProjection.begin(), lXProjection.end());
		int lXBm = std::find(lXProjection.begin(), lXProjection.end(), *lIterator) - lXProjection.begin();

		int lXB0 = lXBm;
		for (; lXProjection[lXB0] >= iCoefficient * lXProjection[lXBm]; lXB0--);

		int lXB1 = lXBm;
		for (; lXProjection[lXB1] >= iCoefficient * lXProjection[lXBm]; lXB1++);
		lXB1--;

		iPlates.push_back(std::make_pair(lXB0, lXB1));
	}
}

std::vector<std::pair<unsigned int, unsigned int>>
gatherProjection(std::vector<std::pair<unsigned int, unsigned int>> iVector) {
	std::vector<std::pair<unsigned int, unsigned int>> lGatheredVector;
	int lEpsilon = 2;
	for (unsigned int i = 0; i < iVector.size() - 1; ++i) {
		int lBornInf1 = iVector[i].first;
		int lBornSup1 = iVector[i].second;
		int lBornInf2 = iVector[i + 1].first;
		int lBornSup2 = iVector[i + 1].second;
		if (lBornInf2 - lBornSup1 <= lEpsilon) {
			lGatheredVector.push_back(std::pair<unsigned int, unsigned int>(lBornInf1, lBornSup2));
		} else {
			lGatheredVector.push_back(std::pair<unsigned int, unsigned int>(lBornInf1, lBornSup1));
			if (i == iVector.size() - 2) {
				lGatheredVector.push_back(std::pair<unsigned int, unsigned int>(lBornInf2, lBornSup2));
			}
		}
	}

	if (iVector.size() == 1) {
		return iVector;
	}

	return lGatheredVector;
}

std::vector<std::pair<unsigned int, unsigned int>>
applyThresholding(std::vector<int> projection, int threshold) {
	std::vector<std::pair<unsigned int, unsigned int>> lLimites;
	bool lIsContinuous = false;
	unsigned int lBornInf = 0;
	unsigned int lBornSup = 0;
	for (unsigned int i = 0; i < projection.size(); ++i) {
		if (projection[i] > threshold) {
			if (!lIsContinuous) {
				lBornInf = i;
				lIsContinuous = true;
				lBornSup = lBornInf;
			}
			if (i == lBornSup + 1) {
				lIsContinuous = true;
				lBornSup = i;
			}
		} else if (lIsContinuous) {
			lIsContinuous = false;
			lLimites.push_back(std::pair<unsigned int, unsigned int> (lBornInf, lBornSup));
		}
	}
	if (lLimites.empty())
		return lLimites;

	std::vector<std::pair<unsigned int, unsigned int>> gatheredBornes;
	gatheredBornes = gatherProjection(lLimites);
	for (unsigned int i = 1; i < lLimites.size(); ++i) {
		gatheredBornes = gatherProjection(gatheredBornes);
	}
	return gatheredBornes;
}

cv::Mat
verticalProject(cv::Mat iImage, std::vector<std::pair<int, int>> iBands) {
	cv::Mat lResultImage(iImage.size(), 0);
	for (std::pair<int, int> lBand : iBands) {
		int lYB0 = lBand.first;
		int lYB1 = lBand.second;
		for (int y = lYB0; y <= lYB1; ++y) {
			for (int x = 0; x < iImage.cols; ++x) {
				lResultImage.at<uchar>(y, x) = iImage.at<uchar>(y, x);
			}
		}
	}
	return lResultImage;
}

void
preProcessingPlate(const cv::Mat iImage, cv::Mat& iResultImage) {
	cv::Mat lMedianImage(iImage.size(), 0);
	flt::medianFilter(iImage, lMedianImage);

	cv::Mat lMedianImage2(iImage.size(), 0);
	flt::medianFilter(lMedianImage, lMedianImage2);

	cv::Mat lMedianImage3(iImage.size(), 0);
	flt::medianFilter(lMedianImage2, lMedianImage3);

	cv::Mat lMedianImage4(iImage.size(), 0);
	flt::medianFilter(lMedianImage3, lMedianImage4);

	cv::Mat lMedianImage5(iImage.size(), 0);
	flt::medianFilter(lMedianImage4, lMedianImage5);

	cv::Mat lMedianImage6(iImage.size(), 0);
	flt::medianFilter(lMedianImage5, lMedianImage6);

	cv::Mat lSobelImage(iImage.size(), 0);
	flt::sobelFilter(lMedianImage6, lSobelImage);

	cv::Mat lOtsuImage(iImage.size(), 0);
	flt::otsuFilter(lSobelImage, lOtsuImage, 190);

	cv::Mat lMedianImage7(iImage.size(), 0);
	flt::medianFilter(lOtsuImage, lMedianImage7);

	flt::medianFilter(lMedianImage7, iResultImage);
}

void
selectBySegmentation(cv::Mat iImage, std::vector<std::pair<int, int>>& iBands, std::vector<std::pair<int, int>>& iPlates) {
	std::vector<std::pair<int, int>> lGoodBands;
	std::vector<std::pair<int, int>> lGoodPlates;
	for (unsigned int i = 0; i < iBands.size(); ++i) {
		std::pair<int, int> lBand = iBands[i];
		std::pair<int, int> lPlate = iPlates[i];
		int lWidth = lPlate.second - lPlate.first;
		int lHeight = lBand.second - lBand.first;

		cv::Mat lSubImage = cv::Mat(iImage, cv::Rect(lPlate.first, lBand.first, lWidth, lHeight));
		preProcessingPlate(lSubImage, lSubImage);

		int lThreshold = 68 * lSubImage.rows;
		std::vector<int> lXProjection = tool::horizontalProjection(lSubImage);
		std::vector<std::pair<unsigned int, unsigned int>> lSegmentedProjection = applyThresholding(lXProjection, lThreshold);
		cv::Mat horizontalProjectionImage = tool::horizontalProjection(lSubImage, lSegmentedProjection);

		unsigned int lLength = lSegmentedProjection.size();
		if (lLength >= 7 && lLength <= 18) {
			lGoodBands.push_back(lBand);
			lGoodPlates.push_back(lPlate);
		}
	}
	iBands = lGoodBands;
	iPlates = lGoodPlates;
}

int
selectByRatio(std::vector<std::pair<int, int>> iBands, std::vector<std::pair<int, int>> iPlates) {
	int lBestROIIndex = -1;
	float lBestRatio = 9000;
	for(unsigned int i = 0; i < iBands.size(); ++i) {
		std::pair<int,int> lBand = iBands[i];
		std::pair<int,int> lPlate = iPlates[i];
		float lRatio = (float)(lPlate.second - lPlate.first) / (float)(lBand.second - lBand.first);
		if (std::abs(lRatio - 4.72) <= lBestRatio) {
			lBestRatio = std::abs(lRatio - 4.72);
			lBestROIIndex = i;
		}
	}
	if (lBestRatio < 11)
		return lBestROIIndex;
	return -1;
}

cv::Mat
printROIs(cv::Mat iImage, std::vector<std::pair<int, int>> iBands, std::vector<std::pair<int, int>> iPlates) {
	cv::Mat lResultImage(iImage.size(), 0);
	for (unsigned int i = 0; i < iBands.size(); ++i) {
		std::pair<int,int> lBand = iBands[i];
		std::pair<int,int> lPlate = iPlates[i];
		for (int y = lBand.first; y <= lBand.second; ++y) {
			for (int x = lPlate.first; x <= lPlate.second; ++x) {
				lResultImage.at<uchar>(y, x) = iImage.at<uchar>(y, x);
			}
		}
	}
	tool::showImage(lResultImage, "ROIs");
	return lResultImage;
}

std::pair<int, int>
computeScore(std::vector<std::pair<int, int>> iBands, std::vector<int> iYProjectionConvolution) {
	int lBestBandIndex = -1;
	int lMaxScore = 0;
	for (unsigned int i = 0; i < iBands.size(); ++i) {
		std::pair<int, int> lBand = iBands[i];
		int lHeight = std::abs(lBand.second - lBand.first);
		std::vector<int>::iterator lIterator = std::max_element(iYProjectionConvolution.begin()
				+ lBand.first, iYProjectionConvolution.begin()
				+ lBand.second);
		int lYBm = std::find(iYProjectionConvolution.begin() + lBand.first, iYProjectionConvolution.begin() + lBand.second, *lIterator) - (iYProjectionConvolution.begin());
		int lAlpha2 = iYProjectionConvolution[lYBm];
		int lAlpha3 = 0;
		for (int j = lBand.first; j < lBand.second - 1; ++j)
			lAlpha3 += iYProjectionConvolution[j];
		int lScore = 0.15 * (1 / lHeight) + 0.25 * (lAlpha2);
		if (lAlpha3 != 0)
			 lScore += 0.4 * (1 / lAlpha3);
		if (lScore > lMaxScore) {
			lMaxScore = lScore;
			lBestBandIndex = i;
		}
	}
	return iBands[lBestBandIndex];
}

void
LPDetection::processDetection(cv::Mat iImage, bool iDebugMode, bool iOcrMode) {
	cv::Mat lGrayScaleImage(iImage.size(), 0);
	flt::grayScaleFilter(iImage, lGrayScaleImage);

	/*
	 * Vertical Axe selection
	 */
	cv::Mat lVerticalImage(iImage.size(), 0);
	flt::verticalEdgeDetection(lGrayScaleImage, lVerticalImage);
	if (iDebugMode)
		tool::showImage(lVerticalImage, "Vertical Edge Detection");

	std::vector<int> lYProjection = tool::verticalProjection(lVerticalImage);
	std::vector<int> lYProjectionConvolution(lYProjection);
	tool::linearizeVector(lYProjection, lYProjectionConvolution, 4);

	std::vector<std::pair<int, int>> lBands;
	selectVerticalPeakProjection(lYProjectionConvolution, lBands, 0.55);

	cv::Mat lVerticalProjectionImage = verticalProject(lGrayScaleImage, lBands);
	if (iDebugMode)
		tool::showImage(lVerticalProjectionImage, "ROI after Y projection");

	/*
	 * Horizontal Axe selection
	 */
	cv::Mat lHorizontalImage(lGrayScaleImage.size(), 0);
	flt::horizontalEdgeDetection(lGrayScaleImage, lHorizontalImage);
	if (iDebugMode)
		tool::showImage(lHorizontalImage, "Horizontal Edge Detection");

	std::vector<std::vector<int>> lXProjections;
	tool::horizontalProjection(lHorizontalImage, lBands, lXProjections);

	std::vector<std::vector<int>> lXProjectionConvolution(lXProjections);
	for (unsigned int i = 0; i < lXProjectionConvolution.size(); ++i)
		tool::linearizeVector(lXProjections[i], lXProjectionConvolution[i], 40);

	std::vector<std::pair<int, int>> lPlates;
	selectHorizontalPeakProjection(lXProjectionConvolution, lPlates, 0.24);

	if (iDebugMode) {
		cv::Mat lROIImage = printROIs(lGrayScaleImage, lBands, lPlates);
		tool::showImage(lROIImage, "ROIs");
	}

	/*
	 * Segmentation selection
	 */
	selectBySegmentation(lGrayScaleImage, lBands, lPlates);

	/*
	 * Ratio selection
	 */
	int lIndexGoodPlate = selectByRatio(lBands, lPlates);

	/*
	 * Plate found ?
	 */
	if (lIndexGoodPlate == -1)
		std::cout << "Plate not found..." << std::endl;
	else {
		std::pair<int, int> lBand = lBands[lIndexGoodPlate];
		std::pair<int, int> lPlate = lPlates[lIndexGoodPlate];

		cv::Mat lFinalImage(lGrayScaleImage.size(), 0.0);

		for (int y = 0; y < lGrayScaleImage.rows; y++) {
			for (int x = 0; x < lGrayScaleImage.cols; x++)
				lFinalImage.at<uchar>(y, x) = 0.0;
		}

		for (int y = lBand.first; y <= lBand.second; ++y) {
			for (int x = lPlate.first; x <= lPlate.second; ++x) {
				lFinalImage.at<uchar>(y, x) = lGrayScaleImage.at<uchar>(y, x);
			}
		}

		tool::showImage(lFinalImage, "Potential Plate");
		std::cout << "Potential plate found!" << std::endl;

		/*
		 * Launch OCR on plate if found
		 */
		if (iOcrMode) {
			TextDetection lTextDetection;
			lTextDetection.processDetection(cv::Mat(lGrayScaleImage, cv::Rect(lPlate.first, lBand.first, lPlate.second - lPlate.first, lBand.second - lBand.first)), iDebugMode, iOcrMode);
		}
	}
}
