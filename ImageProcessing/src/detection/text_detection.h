/*
 * SquareDetection.h
 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */

#ifndef TEXTDETECTION_H_
# define TEXTDETECTION_H_

# include "detection.h"
# include "../filters/filter.h"
# include "baseapi.h"

class TextDetection: public Detection {
public:
	/**
	 * Constructor
	 */
	TextDetection();

	/**
	 * Destructor
	 */
	virtual ~TextDetection();

	/**
	 * \brief Process the detection
	 * \param iImage the original image
	 * \param iDebugMode specifies if the debug mode is active
	 * \param iDebugMode specifies if the orc mode is active
	 */
	virtual void processDetection(cv::Mat iImage, bool iDebugMode, bool iOcrMode);
};

#endif /* TEXTDETECTION_H_ */
