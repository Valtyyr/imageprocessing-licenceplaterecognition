/*
 * TextDetection.cpp
 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */

# include "text_detection.h"

TextDetection::TextDetection() {}

TextDetection::~TextDetection() {}

void
TextDetection::processDetection(cv::Mat iImage, bool iDebugMode, bool iOcrMode) {
	tool::showImage(iImage, "Plate");

	setlocale(LC_NUMERIC, "C");
	cv::Mat lInvertImage(iImage.size(), 0);
	cv::bitwise_not(iImage, lInvertImage);
	tesseract::TessBaseAPI lTessBaseAPI;
	lTessBaseAPI.Init(NULL, "eng", tesseract::OEM_DEFAULT);
	lTessBaseAPI.SetVariable("tessedit_char_whitelist", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	lTessBaseAPI.SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);
	lTessBaseAPI.SetImage((uchar*)lInvertImage.data, lInvertImage.cols, lInvertImage.rows, 1, lInvertImage.cols);

	char* lText = lTessBaseAPI.GetUTF8Text();
	std::cout << lText;
}
