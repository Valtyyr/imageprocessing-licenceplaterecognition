/*
 * Detection.h
 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */

#ifndef DETECTION_H_
#define DETECTION_H_

# include "../main/all.h"

class Detection {
public:
	/**
	 * Constructor
	 */
	Detection();

	/**
	 * Destructor
	 */
	virtual ~Detection();

	/**
	 * \brief Process the detection
	 * \param iImage the original image
	 * \param iDebugMode specifies if the debug mode is active
	 * \param iDebugMode specifies if the orc mode is active
	 */
	virtual void processDetection(cv::Mat iImage, bool iDebugMode, bool iOcrMode) = 0;
};

#endif /* DETECTION_H_ */
