/*
 * Tools.h

 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */
#ifndef TOOLS_H_
# define TOOLS_H_

# include "../main/all.h"

namespace tool {

	/**
	 * \brief Display on the screen an image
	 * This function can only show one image
	 * \param iImage the image to display
	 */
	void showImage(cv::Mat iImage);

	/**
	 * \brief Display on the screen and image
	 * \param iImage the image to display
	 * \param iName the name of the display window
	 */
	void showImage(cv::Mat iImage, std::string iName);

	/**
	 * \brief Apply a Sobel convolution over a specific pixel of an image
	 * \param iImage the original image
	 * \param iX the X coordinate of the pixel to convoluate
	 * \param iY the Y coordinate of the pixel to convoluate
	 */
	int convolutionXSobel(cv::Mat iImage, int iX, int iY);

	/**
	 * \brief Apply a Sobel convolution over a specific pixel of an image
	 * \param iImage the original image
	 * \param iX the X coordinate of the pixel to convoluate
	 * \param iY the Y coordinate of the pixel to convoluate
	 */
	int convolutionYSobel(cv::Mat iImage, int iX, int iY);

	/**
	 * \brief Apply an edge detection convolution over a specific pixel of an image
	 * \param iImage the original image
	 * \param iX the X coordinate of the pixel to convoluate
	 * \param iY the Y coordinate of the pixel to convoluate
	 */
	int convolutionXDetection(cv::Mat iImage, int iX, int iY);

	/**
	 * \brief Apply an edge detection convolution over a specific pixel of an image
	 * \param iImage the original image
	 * \param iX the X coordinate of the pixel to convoluate
	 * \param iY the Y coordinate of the pixel to convoluate
	 */
	int convolutionYDetection(cv::Mat iImage, int iX, int iY);

	/**
	 * \brief Apply a vertical projection
	 * i.e : the sum of each column
	 * \param iVerticalImage the original image
	 */
	std::vector<int> verticalProjection(cv::Mat iVerticalImage);

	/**
	 * \brief Apply an horizontal projection
	 * i.e : the sum of each row
	 * \param iHorizontalImage the original image
	 */
	std::vector<int> horizontalProjection(cv::Mat iHorizontalImage);

	/**
	 * \brief Highlight region detected through the horizontal projection
	 * \param iImage the original image
	 * \param iXLimits the vector thats contents lower and upper bounds of each projection
	 */
	cv::Mat horizontalProjection(cv::Mat iImage, std::vector<std::pair<unsigned int, unsigned int>> iXLimits);

	/**
	 * \brief Apply horizontal projection over many bands of an image
	 * \param iImage the original image
	 * \param iBands vector that contents lower and upper bounds of each bands
	 * \param iXProjection the resulting projection
	 */
	void horizontalProjection(cv::Mat iImage, std::vector<std::pair<int, int>> iBands, std::vector<std::vector<int> >& iXProjections);


	/**
	 * \brief Linearization of a vector
	 * \param iProjection the vector to linearize
	 * \param iResult the resulting vector
	 * \param iWidth heuristical width of the linearization
	 */
	void linearizeVector(std::vector<int> iProjection, std::vector<int>& iResult, int iWidth);

	/**
	 * \brief Apply an insertion sort over an array of int
	 * \param iWindow the int array to sort
	 */
	void insertionSort(int iWindow[]);
}

#endif /* TOOLS_H_ */
