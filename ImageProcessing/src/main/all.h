/*
 * all.h
 *
 *  Created on: 7 mai 2014
 *      Author: valentin
 */

#ifndef ALL_H_
# define ALL_H_

# include <opencv2/core/core.hpp>
# include <opencv2/core/mat.hpp>
# include <opencv2/core/operations.hpp>
# include <opencv2/core/types_c.h>
# include <opencv2/highgui/highgui.hpp>
# include <opencv2/highgui/highgui_c.h>
# include <opencv2/imgproc/imgproc.hpp>
# include <opencv2/imgproc/types_c.h>
# include <opencv2/core/core_c.h>
# include <opencv2/core/types_c.h>

# include <algorithm>
# include <cstdio>
# include <iostream>
# include <sstream>
# include <vector>
# include <cmath>

# include <locale.h>
# include <stddef.h>
# include <string.h>
# include <stdlib.h>

# include "../tools/tools.h"

#endif /* ALL_H_ */
